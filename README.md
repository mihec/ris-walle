## SETUP
change the .bashrc if youre using different computer (IP of the laptop)

battery status from command line:
alias battery='upower -i /org/freedesktop/UPower/devices/battery_BAT0| grep -E "state|to\ full|percentage"'

for rviz *with* map
copy the map to remote_machine/home/team_omicron/maps
export TURTLEBOT_MAP_FILE=/home/team_omicron/maps/erebor_map_1.yaml
also put it to .bashrc 
check amcl_demo.launch for map file location if its pointing to env var

__remote machine__ : roslaunch turtlebot_navigation amcl_demo.launch -- for kinect depth registration launch from kinect_register package
__workstation__ : roslaunch turtlebot_rviz_visualization navigation.launch

### Installing pip on remote machine 
- easy_install --user pip
- PATH=$PATH:~/.local/bin
- when installing with pip use --user flag
- libraries
  - colormath
  - sklearn
  - numpy-quaternion
  

when installing colormath, networkx dependency *might* fail.
- pip install --user 'networkx=2.2'
- pip install --user --no-deps colormath

## COORDINATES
*red* x
*green* y
*blue* z (irrelevant for us)

## LASERS
roslaunch laser_values laser.launch

## Gazebo simulation
comment out the IP address of ROS_MASTER
install ros-kinetic-...-gazebo

## Running Gazebo with map
1. roslaunch turtlebot_gazebo turtlebot_world.launch
1. roslaunch exercise4 amcl_demo_sim.launch
1. roslaunch turtlebot_rviz_launchers view_navigation.launch

Then you can launch any program to interact with the robot.


## REAL RUN
- on robot
    1. bringup minimal launch *or* rins_navigation launch from named robot file (because of angular parameter)
    1. roslaunch rins_navigation amcl_demo.launch (our package)
    1. sudo apt install ros-kinetic-sound-play
    1. rosrun task1 move_around
- on our coputer
    1. rosrun task1 detect_rings
## nodes
detect_rings
detect_3Drings
detect_cylinder
detectercontent
move_around

## TODO
1. ~~map grouping, use two dictionaries~~
1. ~~cylinder colors, lag?, test with robot while he is standing still~~
1. ~~send image for detection, because of waiting~~
1. ~~approach cylinders after classification, because approach point accuracy~~
1. ~~test grasp ring, test pose of 3D approach point with echo~~ ~~got marker now~~
1. test map
1. steal someonse AMCL config
1. ~~id-s for ApproachPoint for cylinders~~
1. look into move_around:194 NoneType new_goal->   if new_goal.goal_id != self.active_goal_id or updated: - Maybe only happening when just the cylinders are running, it says no goal was selected in selectNewGoal in goal manager
1. ~~check status 2, PREEMPTED, wtf is happening (maybe just wait)~~
1. ~~QR code on cylinder, is id even set?~~
1. **get color samples**
1. **aggresive grouping**
1. DO more testing
1. Check corner for cylinder. Possibilities: edge, too litle data points, crank up parameters
