#include "ros/ros.h"

#include <nav_msgs/GetMap.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf2/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <visualization_msgs/Marker.h>
#include <tf2_ros/transform_listener.h>
#include <cmath>


using namespace std;
using namespace cv;

Mat cv_map;
double map_resolution = 0;
geometry_msgs::TransformStamped map_transform;

ros::Publisher marker_pub;
tf2_ros::Buffer buffer;
ros::Subscriber map_sub;

void mapCallback(const nav_msgs::OccupancyGridConstPtr& msg_map) {
    int size_x = msg_map->info.width;
    int size_y = msg_map->info.height;
	
    ROS_INFO("map %d", size_x);
    if ((size_x < 3) || (size_y < 3) ) {
        ROS_INFO("Map size is only x: %d,  y: %d . Not running map to image conversion", size_x, size_y);
        return;
    }

    // resize cv image if it doesn't have the same dimensions as the map
    if ( (cv_map.rows != size_y) && (cv_map.cols != size_x)) {
        cv_map = cv::Mat(size_y, size_x, CV_8U);
    }

    map_resolution = msg_map->info.resolution;
    map_transform.transform.translation.x = msg_map->info.origin.position.x;
    map_transform.transform.translation.y = msg_map->info.origin.position.y;
    map_transform.transform.translation.z = msg_map->info.origin.position.z;

    map_transform.transform.rotation = msg_map->info.origin.orientation;
    ROS_INFO("width %d, height %d, resolution %f", size_x, size_y, map_resolution);
    //tf2::poseMsgToTF(msg_map->info.origin, map_transform);

    const std::vector<int8_t>& map_msg_data (msg_map->data);

    unsigned char *cv_map_data = (unsigned char*) cv_map.data;

    //We have to flip around the y axis, y for image starts at the top and y for map at the bottom
    for (int i = 0; i < size_x*size_y; i++) {
    	switch (map_msg_data[i])
            {
            case -1:
                cv_map_data[i] = 127;
                break;

            case 0:
                cv_map_data[i] = 255;
                break;

            case 100:
                cv_map_data[i] = 0;
                break;
            }
    }

}

tf2::Transform get_transform_matrix(geometry_msgs::TransformStamped transform_msg){

	tf2::Quaternion qua(transform_msg.transform.rotation.x,transform_msg.transform.rotation.y,transform_msg.transform.rotation.z,transform_msg.transform.rotation.w);
	tf2::Vector3 vec3(transform_msg.transform.translation.x,transform_msg.transform.translation.y,transform_msg.transform.translation.z);
	tf2::Transform trans(qua, vec3);
	return trans;

}

bool check_pixel(tf2::Vector3 vector){
	// we need rotation for 180 degrees
	double x = vector.x() * -1.0;
	double y = vector.y() * -1.0;
	
	geometry_msgs::Point pt;
	pt.x = x;
	pt.y = y;
	pt.z = 0;
	
	geometry_msgs::Point transformed_pt;
	tf2::doTransform(pt, transformed_pt, map_transform);
	
	int y_pixel = std::abs((int)(transformed_pt.y / map_resolution));
	int x_pixel = std::abs((int)(transformed_pt.x / map_resolution));
	
	int v = (int)cv_map.at<unsigned char>(y_pixel, x_pixel);
	return (v <= 100);
}

tf2::Vector3 follow_x_axis(tf2::Transform transform_matrix, double step){
	
	double x=0;
	//tf2::Vector3DoubleData vec3(x, 0.0, 0.0);
	tf2::Vector3 vec3(x, 0.0, 0.0);
	
	for(;x<5;x+=step){ 
		vec3.setValue(x, 0.0, 0.0);
		tf2::Vector3 mapVector = transform_matrix(vec3);
		
		if(check_pixel(mapVector)){
			return mapVector;
		}
	}
	
	return vec3;
}

void push_position(int id) {
	try {
		geometry_msgs::TransformStamped trans = buffer.lookupTransform("map", "base_link", ros::Time(0));
		//ROS_INFO("map %f %f", trans.transform.translation.x,trans.transform.translation.y);
		
		tf2::Transform matrix = get_transform_matrix(trans);
		tf2::Vector3 point = follow_x_axis(matrix, 0.05);
		
		visualization_msgs::Marker marker;
		marker.header.frame_id = "/map"; //??
		marker.header.stamp = ros::Time();
		marker.ns = "my_namespace";
		marker.id = id;
		marker.type = visualization_msgs::Marker::CUBE;
		marker.action = visualization_msgs::Marker::ADD;
		marker.pose.position.x = /*trans.transform.translation.x;*/point.x();
		marker.pose.position.y = /*trans.transform.translation.y;*/point.y();
		marker.pose.position.z = /*trans.transform.translation.z;*/point.z();
		marker.pose.orientation.x = 0.0;//trans.transform.rotation.x;
		marker.pose.orientation.y = 0.0;//trans.transform.rotation.y;
		marker.pose.orientation.z = 0.0;//trans.transform.rotation.z;
		marker.pose.orientation.w = 0.0;//trans.transform.rotation.w;
		marker.scale.x = 0.1;
		marker.scale.y = 0.1;
		marker.scale.z = 0.1;
		marker.color.a = 1.0; // Don't forget to set the alpha!
		marker.color.r = 0.5;
		marker.color.g = 0.0;
		marker.color.b = 0.5;
		marker_pub.publish( marker );
	}
	catch (tf2::TransformException &e){
		ROS_WARN("%s", e.what());
		ros::Duration(1.0).sleep();
	}
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "walle");
    ros::NodeHandle n;
    double param_rate;
    std::string param_markers;
    ros::param::get("~rate",param_rate);
    ros::param::get("~markers_topic",param_markers);
    
    map_sub = n.subscribe("map", 10, &mapCallback);
    marker_pub = n.advertise<visualization_msgs::Marker>(param_markers, 5);
    
	tf2_ros::TransformListener listener(buffer); //why?
	
	
	
    ros::Rate rate(param_rate);
    for(int id = 0;ros::ok(); id++) {
        ros::spinOnce();
	push_position(id);	
        rate.sleep();
    }
    return 0;

}
