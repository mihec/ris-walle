#!/usr/bin/env python

import rospy
import actionlib
from actionlib_msgs.msg import GoalStatus
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

def make_goal(x, y):
	goal = MoveBaseGoal()

	#Sending a goal to the to a certain position in the map
	goal.target_pose.header.frame_id = "map"
	goal.target_pose.header.stamp = rospy.Time.now()
	goal.target_pose.pose.position.x = x
	goal.target_pose.pose.position.y = y
	goal.target_pose.pose.orientation.w = 1.0
	return goal


goal_state = GoalStatus.LOST

rospy.init_node('map_navigation', anonymous=False)

ac = actionlib.SimpleActionClient("move_base", MoveBaseAction)

while(not ac.wait_for_server(rospy.Duration.from_sec(2.0))):
              rospy.loginfo("Waiting for the move_base action server to come up")

goals = []

goals.append(make_goal(0.789, -1.08))
goals.append(make_goal(1.58, -0.0133))
goals.append(make_goal(1.24, -2.12))
goals.append(make_goal(0.0821, -1.73))
goals.append(make_goal(-1.55, -1.01))
goals.append(make_goal(1.58, -0.0133))

for i in range(0, len(goals)):

	rospy.loginfo("Sending goal " + str(i))
	ac.send_goal(goals[i])

	while (not goal_state == GoalStatus.SUCCEEDED):

		ac.wait_for_result(rospy.Duration(2))
		goal_state = ac.get_state()
		#Possible States Are: PENDING, ACTIVE, RECALLED, REJECTED, PREEMPTED, ABORTED, SUCCEEDED, LOST.
		rospy.loginfo(goal_state)
		if goal_state == 3 or goal_state == 4:
			rospy.loginfo("The goal "+str(i)+" is unreachable!")
			break		
		elif goal_state == GoalStatus.SUCCEEDED:
			rospy.loginfo("The goal "+str(i)+" was reached!")
		else:
			rospy.loginfo("The goal has not been reached yet! Checking again in 2s.")

	GoalStatus.SUCCEEDED
	goal_state = GoalStatus.LOST
