#!/usr/bin/env python
from __future__ import print_function

import sys
import math
import rospy
import cv2
import numpy as np
import tf2_geometry_msgs
import tf2_ros
import message_filters
import quaternion
import time

#import matplotlib.pyplot as plt
from std_msgs.msg import ColorRGBA

from geometry_msgs.msg import PointStamped, Vector3, Pose
from cv_bridge import CvBridge, CvBridgeError
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import ColorRGBA
from nav_msgs.msg import OccupancyGrid, Odometry
from task1.msg import ApproachPoint

from sklearn.linear_model import RANSACRegressor, LinearRegression

from marker_point_cylinder import MarkerPointCylinder
from image_library import Helpers
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image

class The_Cylinder:
    cylinder_radius = 0.125
    Y_BOT_OFFSET = 10
    Y_TOP_OFFSET = 30
    LASER_OFFSET = -0.15
    
    def __init__(self):
        rospy.init_node('image_converter', anonymous=True)

        # An object we use for converting images between ROS format and OpenCV format
        self.bridge = CvBridge()

        # A help variable for holding the dimensions of the image
        self.dims = (0, 0, 0)

        # Marker array object used for visualizations
        self.sensed_points = []
        self.calculated_points = []
        self.point_num = 0
                
        # Publiser for the visualization markers
        self.markers_pub = rospy.Publisher('markers', MarkerArray, queue_size=1000)
        self.approach_point_pub = rospy.Publisher("approach_point", ApproachPoint, queue_size=10)
 
        # Object we use for transforming between coordinate frames
        self.tf_buf = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buf)
        self.MAX_MSE = 5e-6
        self.MAX_CLUSTER_DISTANCE_NEAR = 1.0
        self.MAX_CLUSTER_DISTANCE_FAR = 1.5

        self.MAX_TWIST = 0.6
        
        # Subscribe to the image and laser scan topic
        self.image_sub = message_filters.Subscriber("/camera/rgb/image_raw", Image)
        self.laser_sub = message_filters.Subscriber('/scan', LaserScan)
        self.odom_sub =  message_filters.Subscriber("/odom", Odometry)
        
        # Filter the messages (paired by time) and register a callback for them
        ts = message_filters.ApproximateTimeSynchronizer([self.image_sub, self.laser_sub, self.odom_sub], 100, 0.5)
        ts.registerCallback(self.laser_callback)

    def laser_callback(self, img_data, laser_scan, odom_data):
        # Get image from cv2_bridge
        if abs(odom_data.twist.twist.angular.z) > self.MAX_TWIST:
            #print("Too spiny ", odom_data.twist.twist.angular.z)
            return
        
        try:
            original_image = self.bridge.imgmsg_to_cv2(img_data, "bgr8")
        except CvBridgeError as e:
            print(e)

            
        # (x,y) -> location of the cylinder
        # angle -> angle of the cylinder based on the robot facing
        MAE, x, y, angle1, angle2 = The_Cylinder.detect_cylinder(laser_scan)

        if MAE < self.MAX_MSE:
            # Get the robot and cylinder locations

            try:
                trans = self.tf_buf.lookup_transform('map', 'base_link', laser_scan.header.stamp, rospy.Duration(1.))
            except:
                print("Error getting transformation")
                return
            
            robot_location = [trans.transform.translation.x, trans.transform.translation.y, 0]
            
            color = The_Cylinder.getCylinderColor(angle1, angle2, original_image)
            print(color)
            
            if color == None or color == "wall": # the cylinder is not in the camera view
                print("Cylinder detected but out of view or it's a wall")
                return

            #print(MAE, " ", x, " ", y, " ", color)
            
            point_s = PointStamped()
            point_s.point.x = y + The_Cylinder.LASER_OFFSET
            point_s.point.y = 0
            point_s.point.z = x
            point_s.header.frame_id = "camera_rgb_optical_frame"
            point_s.header.stamp = laser_scan.header.stamp

            point_world = self.tf_buf.transform(point_s, "map", rospy.Duration(0.05))
            marker = MarkerPointCylinder(point_world.point.x, point_world.point.y, point_world.point.z, robot_location, 10, color, laser_scan.header.stamp)
            self.sensed_points.append(marker)
            self.doGrouping(marker)
            
        marker_array = MarkerArray()
        
        for marker_id in range(1, len(self.calculated_points)+1):
            calc_point = self.calculated_points[marker_id - 1]

            approachMarker = calc_point.getApproachMarker()
            print(calc_point.color)
            approachMarker.id = 2000 + 100 + marker_id
            marker_array.markers.append(approachMarker)
            marker_array.markers.append(calc_point.toMarker(2000 + marker_id, "map", Marker.CYLINDER))
        
        self.markers_pub.publish(marker_array)


    def doGrouping(self, newPoint):
        #check if in group
        closestPoint = None
        
        for point in self.calculated_points:
            distance = point.getDistanceTo(newPoint)
            if(distance <= self.MAX_CLUSTER_DISTANCE_NEAR or (distance <= self.MAX_CLUSTER_DISTANCE_FAR and newPoint.color == point.color)):
                closestPoint = point
                closestDistance = distance
        
        # sensed point is new calculated point
        if(closestPoint == None):
            calcPoint = newPoint.makeCopy()
            calcPoint.addGroupMember(newPoint)
            calcPoint.setId()
            self.calculated_points.append(calcPoint)
            closestPoint = calcPoint
        else:
            closestPoint.addGroupMember(newPoint)
            closestPoint.recalculatePoint()

        approachPose = closestPoint.getCylinderApproachPointPose()
        closestPoint.setApproachPointMarker(approachPose)
            
        cornerApproachPoint = ApproachPoint()
        cornerApproachPoint.goal_id = closestPoint.approachPointId
        cornerApproachPoint.pose = approachPose
        cornerApproachPoint.color = closestPoint.color
        cornerApproachPoint.type = "cylinder"

        self.approach_point_pub.publish(cornerApproachPoint)

        
    @staticmethod
    def getCylinderColor(a1, a2, image): # angle1, angle2, the image
        a1_orig = a1 + 0
        a2_orig = a2 + 0
        
        def to_kinect_angle(angle, k_angle):
            return angle - math.radians(53)
                    
        k_f = 525.0 # kinect focal length in pixels
        k_h = 480.0 # kinect image height
        k_w = 640.0 # kinect image width
        k_fov = math.atan(k_w/(k_f * 2)) * 2
        
        a1 = to_kinect_angle(a1, k_fov)
        a2 = to_kinect_angle(a2, k_fov)
       
        x_position_1 = int(k_w - k_w * (a1 / k_fov))
        x_position_2 = int(k_w - k_w * (a2 / k_fov))
        y_position_upper = int(k_h/2 + The_Cylinder.Y_TOP_OFFSET)
        y_position_bottom = int(k_h/2 + The_Cylinder.Y_BOT_OFFSET)

        x_position_left = min(x_position_1, x_position_2)
        x_position_right = max(x_position_1, x_position_2)        
        
        #print(math.degrees(a1_orig), "->", math.degrees(a1), " ", math.degrees(a2_orig), "->", math.degrees(a2), " ", math.degrees(k_fov))
        #print(x_position_left, " ", x_position_right, " ", y_position_upper, " ", y_position_bottom)

        if x_position_left >= k_w: # cylinder is not in camera view
            return None

        if x_position_right >= k_w:
            x_position_right = int(k_w) - 1

        cropped = image[y_position_bottom:y_position_upper, x_position_left:x_position_right].copy()

        for y in range(y_position_bottom,y_position_upper):
            for x in range(x_position_left,x_position_right):
                image[y,x] = np.array([0,0,0])
        
        #Test print of cropped image
        #cv2.imshow('original image', image)
        #cv2.waitKey(1)
        return Helpers.color_funktion_cylinder(cropped)

    @staticmethod
    def circle(A, B, C):
        def norm(a,b):
            r_x= a[0]-b[0]
            r_y= a[1]-b[1]
            n = math.sqrt(r_x**2+r_y**2)
            return n
        a = norm(C,B)
        b = norm(C,A)
        c = norm(B,A)
        
        def multi(a):
            return (a*a)
        b1 = multi(a) * (multi(b) + multi(c) - multi(a))
        b2 = multi(b) * (multi(a) + multi(c) - multi(b))
        b3 = multi(c) * (multi(a) + multi(b) - multi(c))
        
        P1 = A[0]*b1 + B[0]*b2 + C[0]*b3
        P2 = A[1]*b1 + B[1]*b2 + C[1]*b3
        b = b1 + b2 + b3
        P1 /= b
        P2 /= b
        return [P1,P2]
        
    @staticmethod
    def detect_cylinder(laser_scan):
        x, y, approx_point = Helpers.radian_to_cartesian_cylinders(laser_scan.angle_min, laser_scan.angle_max, laser_scan.angle_increment, laser_scan.ranges)
        if len(x) == 0:
            return 9999, 0, 0, 0, 0

        def check_distance(x ,y , i, nr):
            for l in range(nr, len(x)):
                if math.sqrt((x[i] - x[l])**2 + (y[i] - y[l])**2) > The_Cylinder.cylinder_radius:
                    return l
            return 0
            
        min_mean_square = 9999999
        last_point_in_distance = 0
        center_x_min = 0
        center_y_min = 0
        
        min_i = 0
        min_last = 0
        min_circle = [0,0, The_Cylinder.cylinder_radius]
        min_above = 0
        for i in range(0,len(x),2):
            last_point_in_distance = check_distance(x ,y , i, last_point_in_distance)
            points_in_distance = last_point_in_distance - i
            
            if points_in_distance > 50:
                mean_square = 0
                
                circle = The_Cylinder.circle([x[i],y[i]],[x[i+points_in_distance/2],
                                              y[i+points_in_distance/2]],
                                              [x[i+points_in_distance],y[i+points_in_distance]])
                number_above = 0
                for n in range (i,last_point_in_distance):
                    if y[n] > circle[1]:
                        number_above += 1
                    mean_square = mean_square + (math.sqrt((x[n]-circle[0])**2 + (y[n]-circle[1])**2)-The_Cylinder.cylinder_radius)**2
                
                mean_square = mean_square / points_in_distance
                
                if mean_square < min_mean_square and not number_above >= points_in_distance*0.8:
                    min_mean_square = mean_square
                    center_x_min = circle[0]
                    center_y_min = circle[1]
                    min_i = i
                    min_last = last_point_in_distance
                    min_circle[0] = circle[0]
                    min_circle[1] = circle[1]
                    min_above = number_above

        """
        print(min_mean_square, " ", min_above, min_last - min_i)
        fig, axs = plt.subplots(1,1)
        axs.plot(x[min_i:min_last], y[min_i:min_last], 'x')
        our_circle = plt.Circle((min_circle[0], min_circle[1]), min_circle[2], fill=False)
        axs.add_artist(our_circle)
        axs.set_aspect('equal', 'box')
        plt.show()
        """
        _, angle1 = Helpers.cartesian_to_radian((y[min_i],x[min_i]))
        _, angle2 = Helpers.cartesian_to_radian((y[min_last],x[min_last]))

        return min_mean_square , center_x_min, center_y_min, angle1, angle2
        
            
            

def main():

    cylinder_finder = The_Cylinder()

    try:
        print("We spinning now")
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
