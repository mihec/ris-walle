class Goal:

    APPROACH = 1
    EXPLORATION = 2
    CORNER = 3
    CYLINDER = 4
    
    def __init__(self, goal_id, pose, goal_type, color, visted = False):
        self.goal_id = goal_id
        self.visited = visted
        self.position = pose.position
        self.orientation = pose.orientation
        self.color = color
        self.goal_type = self.convertGoalType(goal_type)
        
    def convertGoalType(self,  argument):
        switcher = {
            "wall": Goal.APPROACH,
            "exploration": Goal.EXPLORATION,
            "corner": Goal.CORNER,
            "cylinder": Goal.CYLINDER
        }
        return switcher.get(argument, 2)
        
    def getGoalTypeInString(self, num):
        nums_to_string = {1:'wall',2:'exploration',3:'corner',4:'cylinder'}
        return nums_to_string[num]
    
    def getDistanceTo(self, otherPose):
        return ((self.position.x - otherPose.position.x)**2 + (self.position.y - otherPose.position.y)**2 + (self.position.z - otherPose.position.z)**2)**0.5
    
    def isApproachPoint(self):
        return self.goal_type == Goal.APPROACH
        
    def isExplorationPoint(self):
        return self.goal_type == Goal.EXPLORATION
        
    def isCornerPoint(self):
        return self.goal_type == Goal.CORNER
        
    def isCylinderPoint(self):
        return self.goal_type == Goal.CYLINDER
    


