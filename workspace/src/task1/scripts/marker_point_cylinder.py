from marker_point import MarkerPoint
from visualization_msgs.msg import Marker, MarkerArray
import numpy as np
from geometry_msgs.msg import PointStamped, Vector3, Pose
from std_msgs.msg import ColorRGBA

class MarkerPointCylinder(MarkerPoint):
    DISTANCE_TO_CYLINDER = 0.5
    
    def __init__(self, x, y, z, robot_location, distance, color, time):
        super(MarkerPointCylinder, self).__init__(x, y, z, distance, color, time)
        self.robo_x = robot_location[0]
        self.robo_y = robot_location[1]
        self.robo_z = robot_location[2]

    def recalculatePoint(self):
        super(MarkerPointCylinder, self).recalculatePoint()
        distanceSum = 0.0
        for point in self.groupMembers:
            distanceSum += 1.0 / point.distance
                
        robo_x = 0.0
        robo_y = 0.0
        robo_z = 0.0
        for point in self.groupMembers:
            #rprint(str(point.x) + " " + str(point.y) + " " +str(point.z))
            robo_x += point.robo_x / point.distance
            robo_y += point.robo_y / point.distance
            robo_z += point.robo_z / point.distance
        
        self.robo_x = robo_x / distanceSum
        self.robo_y = robo_y / distanceSum
        self.robo_z = robo_z / distanceSum
        #print(self.color + " " + str(self.x) + " " + str(self.y) + " " +str(self.z))

    def makeCopy(self):
        return MarkerPointCylinder(self.x, self.y, self.z, [self.robo_x, self.robo_y, self.robo_z], self.distance, self.color, self.time_sensed)
    
    def getCylinderApproachPointPose(self):
        # from cylinder to robot
        vector = np.array([self.robo_x - self.x,
                           self.robo_y - self.y,
                           self.robo_z - self.z])
        vector_norm = vector / np.linalg.norm(vector) * MarkerPointCylinder.DISTANCE_TO_CYLINDER
        
        cylinder = np.array([self.x, self.y, self.z])
        approach_position = cylinder + vector_norm
        
        pose = Pose()
        pose.position.x = approach_position[0]
        pose.position.y = approach_position[1]
        pose.position.z = approach_position[2]

        vector_reverse = vector * -1

        quaternion = self.getQuaternionRotateFromTo([1, 0, 0], [vector_reverse[0], vector_reverse[1], vector_reverse[2]])

        pose.orientation.x = quaternion[0]
        pose.orientation.y = quaternion[1]
        pose.orientation.z = quaternion[2]
        pose.orientation.w = quaternion[3]
        return pose

        
