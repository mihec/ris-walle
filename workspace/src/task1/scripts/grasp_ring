#!/usr/bin/env python

from scipy.optimize import least_squares
import numpy as np
#import matplotlib.pyplot as plt
import rospy
import math
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

from task1.srv import GraspRing, GraspRingResponse
from image_library import Helpers

class Grasper:
    """
    Class which represnts node that is responsible for fine manuvering with **Twist** messages.
    """
    
    pole_offset_x = 0.135
    hanger_offset = 0.165
    roomba_radius = 0.17
    
    rotation_speed = 0.1
    linear_speed = 0.1
    drive_by = 1.0
    
    twist_buffer_size = 10
    angle_divider = 2.12 #gloin: 2.12, fili: 2.3   
    
    # Defines what the service returns
    def grasp_ring(self, req):
        self.start_grasping()
        return GraspRingResponse('Done') 

    
    def __init__(self):
        rospy.init_node('grasp_ring_server')
        self.service = rospy.Service('grasp_ring', GraspRing, self.grasp_ring)
        self.twist_pub = rospy.Publisher("/mobile_base/commands/velocity", Twist, queue_size=Grasper.twist_buffer_size)
    
    
    def start_grasping(self):
        laser_scan = rospy.wait_for_message('/scan', LaserScan)
        print("Got the scan")
        x, y, approx_point = Helpers.radian_to_cartesian(laser_scan.angle_min, laser_scan.angle_max, laser_scan.angle_increment, laser_scan.ranges)
        center_point, slope = Grasper.calculate(x, y, init_params=[-1, approx_point[1], approx_point[0]])
        
        self.start_movig(center_point, slope)
    
    def start_movig(self, center_point, slope):
        
        angle = abs(math.atan(slope)) # not sure
        rotation_angle = math.pi / 2 - angle
        
        offset_distance = (Grasper.hanger_offset + Grasper.pole_offset_x) / math.cos(angle)
        
        n1 = center_point[1] - (slope) * center_point[0]
        n2 = n1 - offset_distance # distance we have to travel
        
        print("move forward " + str(n2))
        self.move(n2, Grasper.linear_speed, linear=True)
        print("rotate " + str(math.degrees(rotation_angle)))
        self.move(rotation_angle / Grasper.angle_divider, Grasper.rotation_speed)
        print("move forward " + str(Grasper.drive_by))
        self.move(Grasper.drive_by, Grasper.linear_speed, linear=True)
        
        
        #plt.plot(0, n2, 'gx')
        #plt.show()
        
    def move(self, path, speed, linear=False):
        x = 0
        z = 0
        if linear:
            x = speed
        else:
            z = speed
            
        twist = Twist()
        twist.linear.x = x
        twist.angular.z = z
        
        t0 = rospy.Time.now().to_sec()
        path_travelled = 0
        while path_travelled < abs(path):
            self.twist_pub.publish(twist)
            t1=rospy.Time.now().to_sec()
            path_travelled = abs(speed * (t1 - t0))
        
        twist.linear.x = 0
        twist.angular.z = 0
        for i in range(0, Grasper.twist_buffer_size):
            self.twist_pub.publish(twist)
        
        print("travelled " + str(path_travelled) + " of " + str(path))
    
    @staticmethod
    def angle_fun(x0, m, b, e):
        if x0 < e:
            return m * (x0 - e) + b
        else:
            return (-1 / m) * (x0 - e) + b

    @staticmethod
    def angle(params, x, y):
        """
        params = [[m], [b], [e]]
        """
        m = params[0]
        b = params[1]
        e = params[2]

        y_calc = np.array([Grasper.angle_fun(xi, m, b, e) for xi in x])
        error = (y_calc - y)
        return error

    @staticmethod
    def angle_jacobian(params, x, y):
        """
        params = [[m], [b], [e]]
        """
        m = params[0]
        b = params[1]
        e = params[2]

        def jacobian_fun(x0):
            if x0 > e:
                return np.array([[x0 - e], [1], [-m]])
            elif x0 == e:
                return np.array([[0], [1], [0]])
            elif x0 < e:
                return np.array([[(x0 - e) / m ** 2], [1], [1 / m]])

        results = [jacobian_fun(xi) for xi in x]
        matrix = np.concatenate(results, axis=1)
        return np.transpose(matrix)

    @staticmethod
    def calculate(x, y, init_params=[0, 0, 0]):
        """
        Fits the perpendicular angle to laser scan points
        :param x: x coordinates of points in metres
        :param y: y coordinates of points in metres
        :param init_params: [m, b, e], [slope, y, x] for initial guess, has to be in the area of true solution
        :return:
        """
        
        def filter_points(center_point, x, y, distance):
            x_center = center_point[0]
            y_center = center_point[1]
            x_filter = []
            y_filter = []
            for i in range(0, len(x)):
                if math.sqrt((x[i] - x_center)**2 + (y[i] - y_center)**2) <= distance:
                    x_filter.append(x[i])
                    y_filter.append(y[i])
            
            return x_filter, y_filter
        
        x, y = filter_points((init_params[2], init_params[1]), x, y, 0.3)
        x = np.array(x)
        y = np.array(y)

        params0 = np.array(init_params)
        result = least_squares(Grasper.angle, params0, args=(x, y))

        x_test = np.linspace(min(x), max(x), 400)
        y_test = [Grasper.angle_fun(xi, result.x[0], result.x[1], result.x[2]) for xi in x_test]

        #plt.plot(x, y, 'o', label="input")
        #plt.plot(x_test, y_test, label="test")
        #plt.plot(init_params[2], init_params[1], 'go')
        #plt.show()
        
        center_point = (result.x[2], result.x[1])
        return center_point, result.x[0]
    
    


if __name__ == "__main__":
    grasper = Grasper()
    try:
        print("We grasping now")
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    
