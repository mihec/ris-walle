#!/usr/bin/env python

import rospy
import sys
import actionlib
import tf2_ros
import random, numpy, math, copy
import uuid
from actionlib_msgs.msg import GoalStatus
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from task1.msg import ApproachPoint, QrDigitMessage, QrDigitStart
from goal import Goal
from geometry_msgs.msg import Pose, Twist
from sensor_msgs.msg import Image
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

from task1.srv import GraspRing
from corner_points_helper import CornerHelper
from corner_point import CornerPoint
from goal_manager import Goal_Manager


class The_Mover:
    def __init__(self):
        rospy.init_node('path_way', anonymous=True)
        #self.RING_NUM = 4
        #self.rings_visited = 0
        self.RAND_GOAL_NUM = 8
        self.RAND_GOAL_MIN_DIST = 1.5       
        
        self.goals = {}
        self.final_locations = {}
        self.DISTANCE_THRESHOLD = 0.20
        self.active_goal_id = None
        self.ac = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        self.cornerHelper = CornerHelper()        

        self.tf_buf = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buf)

        self.goal_manager = Goal_Manager(self.tf_buf)
        
        self.sound_handle = SoundClient()
        self.voice = 'voice_kal_diphone'
        self.volume = 1.0

        self.approach_sub = rospy.Subscriber("/approach_point", ApproachPoint, self.approach_callback)
        self.qrdigit_sub = rospy.Subscriber("/qr_digit_message", QrDigitMessage, self.qrdigit_callback)
        self.qrdigit_pub = rospy.Publisher("/qr_digit_start", QrDigitStart, queue_size=100)
        self.twist_pub = rospy.Publisher("/mobile_base/commands/velocity", Twist, queue_size=100)
        
        self.exploration_limits = ((-1.62,1.62), (-1.75,0.69))#((-2.48,2.53), (-3.02,1.77))
        self.exploration_step = 0.1
        
        init_goal = self.create_random_goal()
        self.goals[init_goal.goal_id] = init_goal
        self.send_goal_wrapper(init_goal)
        print("Initial goal sent")

    def turn_to_Goal(self, x, y, z, w, idx):
        pose = Pose()
        pose.position.x = x
        pose.position.y = y
        pose.position.z = 0
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = z
        pose.orientation.w = w
        return Goal(str(idx),pose, "exploration", "none")
        
    def send_goal_wrapper(self,goal):
        self.ac.cancel_all_goals() # maybe not needed
        self.active_goal_id = goal.goal_id
        self.ac.send_goal(self.make_goal(goal), done_cb=self.doneCb)
        
    def doneCb(self, state, result): #called when the current goal is reached
        old_id = self.active_goal_id
        print("Goal DONE " + old_id)
        print(state)
        
        #http://docs.ros.org/api/actionlib_msgs/html/msg/GoalStatus.html
        if state == GoalStatus.SUCCEEDED or state == GoalStatus.ABORTED or state == GoalStatus.REJECTED:
            print("Goal reached")

            visited_goal = self.goals[self.active_goal_id]
            visited_goal.visited = True
            
            if visited_goal.isCylinderPoint():
                print("Approached a cylinder of color " + visited_goal.color)
                self.sound_handle.say("Found cylinder!", self.voice, self.volume)
                try:
                    image_data = rospy.wait_for_message("/camera/rgb/image_raw", Image) # We take an image here, to make sure we have a still image
                    
                    qstart = QrDigitStart("cylinder_action", visited_goal.color, image_data, self.active_goal_id)
                    self.stay_on_position(10)
                except:
                    print("Problem v isCylinderPoint v move_around")
                # We tell detector_content node to start
                self.qrdigit_pub.publish(qstart)

            if visited_goal.isCornerPoint(): # check if it's a corner point
                print("Will start grasping")
                rospy.wait_for_service('grasp_ring') #wait the service to be advertised
                srv = rospy.ServiceProxy('grasp_ring', GraspRing) #setup a local proxy for the service
                result = srv() #use the service
                self.sound_handle.say("Picked a ring of color " + visited_goal.color, self.voice, self.volume)
                
                self.goal_manager.setPicked3dFlag()
                
            elif visited_goal.isApproachPoint():
                print("Approached a circle of color " + visited_goal.color)
                self.sound_handle.say("Found one", self.voice, self.volume)
                try:
                    image_data = rospy.wait_for_message("/camera/rgb/image_raw", Image) # We take an image here, to make sure we have a still image
                    
                    qstart = QrDigitStart("approach_action", visited_goal.color, image_data, self.active_goal_id)
                    self.stay_on_position(10)
                except:
                   print("Problem v isApproachPoint v move_around")
                # We tell detector_content node to start
                self.qrdigit_pub.publish(qstart)

            elif state == GoalStatus.SUCCEEDED: # this is exploration goal, there is possibility it is aborted
                print("Looking around")
                self.roate_on_position(math.pi * 2, 0.4)
                print("I have seen things, horrible things")
                

            init_goal = self.create_random_goal()
            self.goals[init_goal.goal_id] = init_goal
                
            next_goal = self.goal_manager.selectNextGoal(self.goals)
            if(next_goal != None):
                print("Sending new in CB " + old_id + " -> " + next_goal.goal_id + " position of new: ", next_goal.position)
                self.send_goal_wrapper(next_goal)
            else:
                print("Over and out")
                rospy.signal_shutdown("Robot has finished his path... for now")

        elif state == GoalStatus.PREEMPTED:
            # special case
            visited_goal = self.goals[self.active_goal_id]
            visited_goal.visited = True
            
            init_goal = self.create_random_goal()
            self.goals[init_goal.goal_id] = init_goal
            next_goal = self.goal_manager.selectNextGoal(self.goals)
            self.send_goal_wrapper(next_goal)
            print("Goal preempted")
        
    def qrdigit_callback(self, msg):
        print(msg.action)
        # msg has action, color and pose
        if msg.action == "color_of_ring":
            # Handles receiving the color of the map ring, color is the 3d ring color
            # We get this after approaching a cylinder
            self.goal_manager.setMapAnd3DColor(msg.color)
            
        elif msg.action == "color_of_cylinder":
            # Handles receiving the color of the cylinder we have to approach
            # We get this after approaching both a data set ring and a digits ring
            print("MA: Got the color of the cylinder")
            self.goal_manager.setCylinderColor(msg.color)
            
        elif msg.action == "map_from_ring":
            # Handles receiving the color of a wall ring with the map. We receive a pose that tells us the location of the X on the map and a color of the wall ring we used.
            # We get this after reading a wall ring with a map.
            self.goal_manager.final_locations[msg.color] = Goal(12345, msg.pose, "wall", msg.color)
            
        elif msg.action == "reset_goal":
            self.goals = self.goal_manager.resetGoal(msg.goal_id, self.goals)
            
        else:
            print("Did not receive a proper action name in qr_digit callback in the move_around file")
            
    def approach_callback(self, msg):
        def measure_distance(goal1,pose2):
            return math.sqrt((goal1.position.x - pose2.position.x)**2 + (goal1.position.y - pose2.position.y)**2)
        
        new_pose = msg.pose
        new_id = msg.goal_id
        new_type = msg.type
        new_color = msg.color
        
        #if new_type == 'corner':
            #new_pose = self.cornerHelper.getClosestPoint(new_pose.position.x, new_pose.position.y)
        
        updated = False
        
        if new_id in self.goals:
            if self.goals[new_id].visited and self.goals[new_id].isCornerPoint(): # we make sure that we pick up the ring if we see it
                #self.rings_visited -= 1
                self.goals[new_id].visited = False 

            if measure_distance(self.goals[new_id], new_pose) > self.DISTANCE_THRESHOLD:
                updated = True
                print("Updating goal " + new_id)
                self.goals[new_id] = Goal(new_id, new_pose, new_type, new_color)
        else:
            print("We got a new goal " + new_id)
            self.goals[new_id] = (Goal(new_id, new_pose, new_type, new_color))
            
        # Select the new active goal
        new_goal = self.goal_manager.selectNextGoal(self.goals)
        if new_goal.goal_id != self.active_goal_id or updated:
            self.send_goal_wrapper(new_goal)
        
    #used to convert our Goal object into a MoveBaseGoal that we can then send to the ac
    def make_goal(self, goal_obj):
        goal = MoveBaseGoal()
        
        #Sending a goal to the to a certain position in the map
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()

        goal.target_pose.pose.position = goal_obj.position
        goal.target_pose.pose.orientation = goal_obj.orientation
        return goal

    def create_random_goal(self):
        def make_one():
            x = random.uniform(self.exploration_limits[0][0], self.exploration_limits[0][1])
            y = random.uniform(self.exploration_limits[1][0], self.exploration_limits[1][1])
            goal = self.turn_to_Goal(x, y, 0, 1, str(uuid.uuid4()))
            return goal

        trans = self.tf_buf.lookup_transform('map', 'base_link', rospy.Time(), rospy.Duration(1000.))
        robot_location = [trans.transform.translation.x, trans.transform.translation.y]
        
        def distance(roboloc, goal):
            return math.sqrt((roboloc[0] - goal.position.x)**2 + (roboloc[1] - goal.position.y)**2)
        
        rand_goals = list()
        for i in range(0, self.RAND_GOAL_NUM):
            rand_goals.append(make_one())

        optimal_goal = rand_goals[0]
        optimal_distance = distance(robot_location, optimal_goal)
        for goal in rand_goals:
            goal_distance = distance(robot_location, goal)
            if self.RAND_GOAL_MIN_DIST < goal_distance < optimal_distance:
                optimal_goal = goal
                optimal_distance = goal_distance

        return optimal_goal

    def roate_on_position(self, radians, speed):
        twist = Twist()
        twist.angular.z = speed
        
        t0 = rospy.Time.now().to_sec()
        path_travelled = 0
        while path_travelled < abs(radians):
            self.twist_pub.publish(twist)
            t1=rospy.Time.now().to_sec()
            path_travelled = abs(speed * (t1 - t0))
        
        twist.angular.z = 0
        for i in range(0, 100):
            self.twist_pub.publish(twist)
        
        print("travelled " + str(path_travelled) + " of " + str(radians))

    def stay_on_position(self, seconds):
        twist = Twist()
        
        t0 = rospy.Time.now().to_sec()
        while rospy.Time.now().to_sec() - t0 < seconds:
            self.twist_pub.publish(twist)
        
        
def main():
    mover = The_Mover()
    
    try:
        print("We spinning now")
        rospy.spin()
        
    except KeyboardInterrupt:
        print("Shutting down")


if __name__ == '__main__':
    main()
