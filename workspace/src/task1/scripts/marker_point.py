import rospy
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import PointStamped, Vector3, Pose

import math
import uuid
import numpy as np
import quaternion
from sklearn.linear_model import RANSACRegressor, LinearRegression

class MarkerPoint(object):
    def __init__(self, x, y, z, distance, color, time):
        self.x = x
        self.y = y
        self.z = z
        self.distance = distance
        self.color = color
        self.groupMembers = []
        self.approachPoint = None
        self.approachPointId = None
        self.time_sensed = time
        self.quaternion_rotation = [0,0,0,0]
        self.approachMarker = None
        
        self.RANSAC_BOX_SIZE = 10 # in pixels
        self.RANSAC_SAMPLE_SIZE = 2
        self.APPROACH_DISTANCE = 0.6 # meters?
        self.Error_distance = 2 #pixel
        self.approch_distance = 10 #pixel
        self.Ring3D_BOX_SIZE = 20 # in pixels
    
    
    def setApproachPoint(self, marker):
        if(self.approachPoint == None):
            marker.setId()
        else:
            marker.approachPointId = self.approachPoint.approachPointId
        self.approachPoint = marker

    def setId(self):
        self.approachPointId = str(uuid.uuid4())
    
    def setApproachPointMarker(self, approach_pose):
        marker = Marker()
        marker.header.frame_id = "map"
        marker.pose = approach_pose
        marker.type = Marker.ARROW
        marker.action = Marker.ADD
        marker.frame_locked = False
        marker.id = 1000
        marker.scale = Vector3(0.1, 0.1, 0.1)
        marker.color = ColorRGBA(*[0.498,0,1,1])

        self.approachMarker = marker

    def getApproachMarker(self):
        return self.approachMarker
        
    def setQuaternion(self, quaternion):
        self.quaternion_rotation = quaternion
    
    def colorname_to_RGBA(self):
            if self.color == "red":
                return [1,0,0,1]
            elif self.color == "green":
                return [0,1,0,1]
            elif self.color == "blue":
                return [0,0,1,1]
            elif self.color == "yellow":
                return [1,0.75,0,1]
            elif self.color == "black":
                return [0.2,0.2,0.2,1]
            elif self.color == "purple":
                return [0.498,0,1,1]
                
    def getDistanceTo(self, otherPoint):
        return ((self.x - otherPoint.x)**2 + (self.y - otherPoint.y)**2 + (self.z - otherPoint.z)**2)**0.5
        
    def getPose(self):
        pose = Pose()
        pose.position.x = self.x
        pose.position.y = self.y
        pose.position.z = self.z
        pose.orientation.x = self.quaternion_rotation[0]
        pose.orientation.y = self.quaternion_rotation[1]
        pose.orientation.z = self.quaternion_rotation[2]
        pose.orientation.w = self.quaternion_rotation[3]
        return pose
        
    def addGroupMember(self, member):
        self.groupMembers.append(member)
        
    def recalculatePoint(self):
        distanceSum = 0.0
        colorVotes = [[0, "red"], [0, "green"], [0, "blue"], [0, "black"], [0, "yellow"]]
        for point in self.groupMembers:
            distanceSum += 1.0 / point.distance
            if point.color == "red":
                colorVotes[0][0] += 1.0 / point.distance
            elif point.color == "green":
                colorVotes[1][0] += 1.0 / point.distance
            elif point.color == "blue":
                colorVotes[2][0] += 1.0 / point.distance
            elif point.color == "black":
                colorVotes[3][0] += 1.0 / point.distance
            elif point.color == "yellow":
                colorVotes[4][0] += 1.0 / point.distance
        
        maxVotes = 0
        newColor = "none"
        for vote in colorVotes:
            if vote[0] >= maxVotes:
                newColor = vote[1]
                maxVotes = vote[0]
        
        self.color = newColor
        x = 0.0
        y = 0.0
        z = 0.0
        for point in self.groupMembers:
            #print(str(point.x) + " " + str(point.y) + " " +str(point.z))
            x += point.x / point.distance
            y += point.y / point.distance
            z += point.z / point.distance
        
        self.x = x / distanceSum
        self.y = y / distanceSum
        self.z = z / distanceSum
        #print(self.color + " " + str(self.x) + " " + str(self.y) + " " +str(self.z))
      
      
    def toHomogenousVector(self):
        return [self.x, self.y, self.z, 1]
        
        
    def toVector(self):   
        return [self.x, self.y, self.z, 1]
        
        
    def toMarker(self, markerId, frame_id, markerType):
        marker = Marker()
        marker.header.frame_id = frame_id
        marker.pose = self.getPose()
        marker.type = markerType
        marker.action = Marker.ADD
        marker.frame_locked = False
        marker.id = markerId
        marker.scale = Vector3(0.1, 0.1, 0.1)
        marker.color = ColorRGBA(*self.colorname_to_RGBA())
        return marker
        
        
    def makeCopy(self):
        return MarkerPoint(self.x, self.y, self.z, self.distance, self.color, self.time_sensed)
    
    
    def getVectorTo(self, markerTo):
        return [markerTo.x - self.x, markerTo.y - self.y, markerTo.z - self.z]
    
    
    def getQuaternionRotateFromTo(self, origin_vector, dest_vector):
        # from some guy on https://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another
        # Quaternion q;
        # vector a = crossproduct(v1, v2);
        # q.xyz = a;
        # q.w = sqrt((v1.Length ^ 2) * (v2.Length ^ 2)) + dotproduct(v1, v2);
        
        v2 = np.array([dest_vector])
        v1 = np.array([origin_vector])
        a = np.cross(v1, v2)
        l1 = np.linalg.norm(v1)
        l2 = np.linalg.norm(v2)
        w = math.sqrt(l1**2 * l2**2) + np.dot(v1, np.transpose(v2))
        return [a[0,0], a[0,1], a[0,2], w[0,0]]
            
        
    def get_approach_point(self, ringObject):       
        homogenous = self.toHomogenousVector()
        homogenous = np.array([homogenous])
        homogenous = np.transpose(homogenous)
        meter_coordinates = np.dot(ringObject.transformation_matrix, homogenous)
        meter_coordinates = meter_coordinates / meter_coordinates[3, 0] # because of homography
        pixel_coordinates = meter_coordinates / ringObject.map_resolution
        
        p_y = -1* int(pixel_coordinates[1,0])
        p_x = -1* int(pixel_coordinates[0,0])

        cut_out = ringObject.map[p_y-self.RANSAC_BOX_SIZE:p_y+self.RANSAC_BOX_SIZE, p_x-self.RANSAC_BOX_SIZE:p_x+self.RANSAC_BOX_SIZE]
        
        x_black = []
        y_black = []
        
        for y in range(-self.RANSAC_BOX_SIZE, self.RANSAC_BOX_SIZE):
            for x in range(-self.RANSAC_BOX_SIZE, self.RANSAC_BOX_SIZE):
                if ringObject.map[p_y + y, p_x + x] == 0:
                    x_black.append(p_x + x)
                    y_black.append(p_y + y)
        
        x_black = np.array(x_black)
        y_black = np.array(y_black)
        x_black = np.reshape(x_black, (-1, 1))
        y_black = np.reshape(y_black, (-1, 1))               
        reg = RANSACRegressor(min_samples=self.RANSAC_SAMPLE_SIZE).fit(x_black, y_black)
        
        m = reg.estimator_.coef_[0,0] # slope
        t = reg.estimator_.intercept_[0] # intercept            
        
        # point on line, perpendicular to marker        
        if m == 0:
            m = 0.01

        x_s = ((p_x / m) + p_y - t) / (m + 1.0 / m)
        y_s = m * x_s + t
        
        vector = [x_s - p_x, y_s - p_y] # from point on line to marker
        
        factor = (self.APPROACH_DISTANCE/ringObject.map_resolution)/(math.sqrt(vector[0]**2 + vector[1]**2)) # extend vector to approaching distance size
        
        vector[0] *= factor
        vector[1] *= factor
        
        # candidate points
        candidates = np.array([[x_s - vector[0], x_s + vector[0]], [y_s - vector[1], y_s + vector[1]]])
        
        candidates = candidates * ringObject.map_resolution * (-1) # back to meters in picture
        
        candidates = np.concatenate((candidates, np.array([[0,0], [1,1]])), axis=0)
        
        map_candidates = np.dot(ringObject.transformation_matrix_back, candidates) # back to map space
        map_candidate1 = map_candidates[0:3,0]
        map_candidate2 = map_candidates[0:3,1]
        
        return MarkerPoint(map_candidate1[0], map_candidate1[1], map_candidate1[2], 0, "purple", self.time_sensed), MarkerPoint(map_candidate2[0], map_candidate2[1], map_candidate2[2], 0, "purple", self.time_sensed)
        
    def get_Corner_Approch_Point(self, ringObject):
        print("Ring coordinates in Marker point ", [self.x, self.y])
        homogenous = self.toHomogenousVector()
        homogenous = np.array([homogenous])
        homogenous = np.transpose(homogenous)
        meter_coordinates = np.dot(ringObject.transformation_matrix, homogenous)
        meter_coordinates = meter_coordinates / meter_coordinates[3, 0] # because of homography
        pixel_coordinates = meter_coordinates / ringObject.map_resolution
        
        p_y = -1* int(pixel_coordinates[1,0])
        p_x = -1* int(pixel_coordinates[0,0])

        cut_out = ringObject.map[p_y-self.Ring3D_BOX_SIZE:p_y+self.Ring3D_BOX_SIZE, p_x-self.Ring3D_BOX_SIZE:p_x+self.Ring3D_BOX_SIZE]
        np.savetxt("cutout.txt", cut_out, fmt="%3d") 
        black_points = []
        min_distance = 9999
        min_distance_point=[0,0]
        
        for y in range(-self.Ring3D_BOX_SIZE, self.Ring3D_BOX_SIZE):
            for x in range(-self.Ring3D_BOX_SIZE, self.Ring3D_BOX_SIZE):
                if ringObject.map[p_y + y, p_x + x] == 0:
                    black_points.append([p_x + x, p_y + y])
                    if x**2 + y**2 < min_distance:
                        min_distance = x**2 + y**2
                        min_distance_point=[x + p_x,y + p_y]
        
        # make vector and rotate for 90 degrees to the right 
        wall = [min_distance_point[1] - p_y, -(min_distance_point[0] - p_x)]
        norm = math.sqrt(wall[0]**2 + wall[1]**2)
        wall = [wall[0] / norm, wall[1] / norm]

        def distance_to_line(point, min_distance_point, wall):
            # distance point to line (min_distance_point,wall)
            distance = (point[0] - min_distance_point[0]) * wall[1] - (point[1] - min_distance_point[1]) * wall[0]
            return distance**2

        def right_direction(point, min_distance_point, wall):
            reference_point = [min_distance_point[0] + wall[0], min_distance_point[1] + wall[1]]
            return distance_points(point, min_distance_point) > distance_points(point, reference_point)
        
        def distance_points(p1, p2):
            return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)
    
        max_distance = 0
        corner_point = [min_distance_point[0], min_distance_point[1]]
        found = False
        for point in black_points:
            if right_direction(point, min_distance_point, wall) \
               and distance_to_line(point,min_distance_point,wall) < self.Error_distance \
               and distance_points(point, min_distance_point) > max_distance:
                 max_distance = distance_points(point, min_distance_point)
                 corner_point = point
                 found = True
        
        if not found:
            print("ERROR Didn't find any corner point, using closest wall point instead")
            print("min dist ", min_distance_point, "wall", wall)
            return None
        
        approch_direction = [-(wall[0] + wall[1]), wall[0] - wall[1]]
        norm = math.sqrt(approch_direction[0]**2 + approch_direction[1]**2)
        approch_direction = [approch_direction[0] / norm, approch_direction[1] / norm]
        approach_corner = [corner_point[0] - self.approch_distance*approch_direction[0], corner_point[1] - self.approch_distance*approch_direction[1]]
        
        # candidate points
        candidates = np.array([[approach_corner[0], approch_direction[0], 0], 
                               [approach_corner[1], approch_direction[1], 0]])
        
        candidates = candidates * ringObject.map_resolution * (-1) # back to meters in picture
        
        candidates = np.concatenate((candidates, np.array([[0,0,0], [1,1,1]])), axis=0)
        
        map_candidates = np.dot(ringObject.transformation_matrix_back, candidates) # back to map space
        map_approach_corner = map_candidates[0:3,0]
        map_approch_direction = map_candidates[0:3,1]
        map_coordinate_origin = map_candidates[0:3,2]
        
        # we need to fix vector after transfomation, we are transforming points not vectors
        map_approch_direction = [map_approch_direction[0] - map_coordinate_origin[0], map_approch_direction[1] - map_coordinate_origin[1], 0]        

        quaternion = self.getQuaternionRotateFromTo([1, 0, 0], map_approch_direction)
        corner_pose = Pose()
        corner_pose.position.x = map_approach_corner[0]
        corner_pose.position.y = map_approach_corner[1]
        corner_pose.position.z = 0
        corner_pose.orientation.x = quaternion[0]
        corner_pose.orientation.y = quaternion[1]
        corner_pose.orientation.z = quaternion[2]
        corner_pose.orientation.w = quaternion[3]
        
        testing_points = np.array([[min_distance_point[0], corner_point[0], approach_corner[0]],
                                   [min_distance_point[1], corner_point[1], approach_corner[1]]])

        testing_points = testing_points * ringObject.map_resolution * (-1)
        testing_points = np.concatenate((testing_points, np.array([[0,0,0], [1,1,1]])), axis=0)
        #print(np.dot(ringObject.transformation_matrix_back, testing_points))

        return corner_pose
