#!/usr/bin/env python
from colormath.color_objects import LabColor, sRGBColor
from colormath.color_diff import delta_e_cie2000
from colormath.color_conversions import convert_color
from geometry_msgs.msg import Pose
import matplotlib.pyplot as plt
import numpy as np
import math
import cv2
import operator
import rospy
import matplotlib.pyplot as plt

class Helpers:
    laser_offset_x = -0.025
    laser_offset_y = 0.095

    laser_cylinders_offset_y = 0
    laser_cylinders_offset_x = 0
    
    laser_rgb_x_offset = 0.03
    
    @staticmethod
    def radian_to_cartesian(angle_min,angle_max,angle_increment,distance):
        x = []
        y = []
        check = True
        angle = angle_min
        distance_min = float("inf")
        coresponding_angle = 0
        max_nr = int((angle_max-angle_min)/angle_increment)
        for count in range(0,max_nr):
            if not math.isnan(distance[count]):
                if distance[count]<distance_min:
                    distance_min=distance[count]
                    coresponding_angle = angle
                    
                angle=angle+angle_increment
                x.append(math.cos(math.pi/2+angle)*distance[count] + Helpers.laser_offset_x)
                y.append(math.sin(math.pi/2+angle)*distance[count] - Helpers.laser_offset_y)
                
        distance_min_x = (math.cos(math.pi/2+coresponding_angle)*distance_min + Helpers.laser_offset_x)
        distance_min_y = (math.sin(math.pi/2+coresponding_angle)*distance_min - Helpers.laser_offset_y)
        return x, y, (distance_min_x, distance_min_y)


    @staticmethod
    def radian_to_cartesian_cylinders(angle_min,angle_max,angle_increment,distance):
        x = []
        y = []
        check = True
        angle = angle_min
        distance_min = float("inf")
        coresponding_angle = 0
        max_nr = int((angle_max-angle_min)/angle_increment)
        for count in range(0,max_nr):
            if not math.isnan(distance[count]):
                if distance[count]<distance_min:
                    distance_min=distance[count]
                    coresponding_angle = angle

                angle=angle+angle_increment
                y.append(math.cos(math.pi/2+angle)*distance[count] + Helpers.laser_cylinders_offset_y)
                x.append(math.sin(math.pi/2+angle)*distance[count] - Helpers.laser_cylinders_offset_x)

        distance_min_y = (math.cos(math.pi/2+coresponding_angle)*distance_min + Helpers.laser_cylinders_offset_y)
        distance_min_x = (math.sin(math.pi/2+coresponding_angle)*distance_min - Helpers.laser_cylinders_offset_x)
        return x, y, (distance_min_x, distance_min_y)

    
    @staticmethod
    def cartesian_to_radian(point):
        point = (point[0] + Helpers.laser_cylinders_offset_x, point[1] + 4*Helpers.laser_cylinders_offset_y)
        r = math.sqrt(point[0]**2+point[1]**2)
        if point[1] > 0:
            angle = math.acos(point[0]/r)
        else: angle = -math.acos(point[0]/r)
        return r, angle
    
    @staticmethod
    def depth_mask_img(cv_image,depth_image,max_distance):
        """
        Receives depth image and sets to black everything that is further than *max_distance*.
        The result is a black image with clearly visible white rings.
        """
        
        mask = np.logical_or(depth_image > max_distance * 1000, depth_image == 0) # True where statement true
        mask_reverse = ~mask  # True where we want to retain value
        mask_reverse = mask_reverse.astype(np.uint8)

        mask = mask.astype(np.uint8)
        mask_white = mask * 255
        
        return mask_white    
    
    @staticmethod
    def check_fraction(half_axis, e1, e2, CIRCLE_FRACTION, ERROR):            
        val1 = e1[1][half_axis]     
        val2 = e2[1][half_axis]     
        if val1 == 0:              
            val1 = 0.001      
                                   
        if val2 == 0:              
            val2 = 0.001           
    
        circle_fraction = max(val1, val2) / min(val1, val2)
        return CIRCLE_FRACTION*(1 - ERROR) <= circle_fraction <= CIRCLE_FRACTION*(1 + ERROR)
            
    @staticmethod
    def check_eccentricity(e1, e2, MAX_ECCENTRICITY):
        eccentricity1 = math.sqrt(1 - e1[1][0]/e1[1][1])
        eccentricity2 = math.sqrt(1 - e2[1][0]/e2[1][1])
        return eccentricity1 < MAX_ECCENTRICITY and eccentricity2 < MAX_ECCENTRICITY
    
    @staticmethod
    def check_size(e, MAX_SIZE, MIN_SIZE):
        return MAX_SIZE > e[1][0] > MIN_SIZE and MAX_SIZE > e[1][1] > MIN_SIZE
    
    @staticmethod
    def ellipse(x, y, x_c, y_c, a, b, fi):
        # https://math.stackexchange.com/questions/426150/what-is-the-general-equation-of-the-ellipse-that-is-not-in-the-origin-and-rotate
        x_diff = x - x_c
        y_diff = y - y_c
        return (x_diff * math.cos(fi) + y_diff * math.sin(fi))**2/(a**2) + (x_diff * math.sin(fi) - y_diff * math.cos(fi))**2/(b**2)
    
    @staticmethod
    def contour_mean_square(contour, x_c, y_c, a, b, fi):
        count = 0
        for c in contour:
            count = count + (1-Helpers.ellipse(c[0][0], c[0][1], x_c, y_c, a, b, fi))**2
        count = count / len(contour)
        return count
    
    @staticmethod
    def distance(a,b, power):
        a_RGB = sRGBColor(a[2],a[1],a[0])
        b_RGB = sRGBColor(b[2],b[1],b[0])
        a_lab = convert_color(a_RGB, LabColor)
        b_lab = convert_color(b_RGB, LabColor)
        color_dist = (a_lab.lab_l-b_lab.lab_l)**power + (a_lab.lab_a-b_lab.lab_a)**power + (a_lab.lab_b-b_lab.lab_b)**power
        return color_dist
        
    @staticmethod
    def distance_euclid(a,b, power):
        #color_dist = (a[0]-b[0])**power + (a[1]-b[1])**power + (a[2]-b[2])**power
        return Helpers.distance(a,b,power)
        #return color_dist
        
    @staticmethod
    def distance_euclid_2(a,b, power): # takes 2d points
        color_dist = (a[0]-b[0])**power + (a[1]-b[1])**power
        return math.sqrt(color_dist)
            
    @staticmethod
    #gets map form image. Red pixels are used to decide if there is a map.
    def get_map(e_big, e_small, image):
        if (e_big[1][0] < e_small[1][0]):
            e_small = e_big
        # get inner circle of Ring -5 pixel
        a = e_small[1][0]/2-5
        b = e_small[1][1]/2-5
        
        maxx = max(b,a)
        black_pix = []
        red_pix = []
        
        def color_evaluation(pix):
            # TODO check real white color values in image
            colors = [([150,100,130],"red"), ([60,60,80], "black"), ([200,200,200], "white")]
            distances = map(lambda c: (Helpers.distance_euclid(pix, c[0], 2), c[1]), colors)
            return min(distances, key = lambda d: d[0])[1]
            
        for x in range(int(e_big[0][0]-maxx),int(e_big[0][0]+maxx)+1):
            for y in range(int(e_big[0][1]-maxx),int(e_big[0][1]+maxx)+1):
                # checks if pixel is in inner cicle 
                if Helpers.ellipse(x, y, e_small[0][0], e_small[0][1], a, b, math.radians(e_small[2])) < 1:
                    # checks color for every pixel (white, black and red)
                    # propobly slow -> c++ ? 
                    pix_color = color_evaluation([image[y][x][0],image[y][x][1],image[y][x][2]])
                    if pix_color == "black":
                        black_pix.append((x,y))
                    elif pix_color == "red":
                        red_pix.append((x,y))
        if len(red_pix) < 5:
            print("there is no map")
            return False, black_pix, red_pix
        return True, black_pix, red_pix

    
    @staticmethod
    def get_candidates(original, dims,
                       CROP_Y,
                       DISTANCE_THRESHOLD,
                       CIRCLE_FRACTION,
                       ERROR,
                       MAX_ECCENTRICITY,
                       MAX_SIZE,
                       MIN_SIZE,
                       MSE_ELLIPSE):
        
        cv_image = original[CROP_Y:(dims[0] - CROP_Y), 0:dims[1]].copy()

        # Tranform image to grayscale
        gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)

        # Do histogram equlization
        img = cv2.equalizeHist(gray)
        
        img = cv2.medianBlur(img, 5)
        # img = cv2.GaussianBlur(img, (5,5), 5)
        
        # Binarize the image        
        thresh = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,31,2)

        # Extract contours
        im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        # Example how to draw the contours
        cv2.drawContours(img, contours, -1, (255, 0, 0), 3)
        
        # Fit elipses to all extracted contours
        elps = []
        for cnt in contours:
            #     print cnt
            #     print cnt.shape
            if cnt.shape[0] >= 20:
                ellipse = cv2.fitEllipse(cnt)
                #cv2.ellipse(img, ellipse, (0, 255, 0), 2)
                elps.append((ellipse, cnt))

        
        # Find two elipses with same centers
        candidates = []
        
        def fix_coordinates(e):
            return ((e[0][0], e[0][1] + CROP_Y), e[1], e[2])
        
        for n in range(len(elps)):
            for m in range(n + 1, len(elps)):
                e1 = elps[n][0]
                e2 = elps[m][0]
                cnt1 = elps[n][1]
                cnt2 = elps[m][1]
                dist = np.sqrt(((e1[0][0] - e2[0][0]) ** 2 + (e1[0][1] - e2[0][1]) ** 2))

                if dist < max(e1[1][0],e1[1][1],e2[1][0],e2[1][1]) * DISTANCE_THRESHOLD\
                   and Helpers.check_fraction(0, e1, e2, CIRCLE_FRACTION, ERROR)\
                   and Helpers.check_fraction(1, e1, e2, CIRCLE_FRACTION, ERROR)\
                   and Helpers.check_eccentricity(e1, e2, MAX_ECCENTRICITY)\
                   and Helpers.check_size(e1, MAX_SIZE, MIN_SIZE)\
                   and Helpers.check_size(e2, MAX_SIZE, MIN_SIZE)\
                   and Helpers.contour_mean_square(cnt1, e1[0][0], e1[0][1], e1[1][0]/2, e1[1][1]/2, math.radians(e1[2])) < MSE_ELLIPSE\
                   and Helpers.contour_mean_square(cnt2, e2[0][0], e2[0][1], e2[1][0]/2, e2[1][1]/2, math.radians(e2[2])) < MSE_ELLIPSE:
                    e1 = fix_coordinates(e1)
                    e2 = fix_coordinates(e2)
                    c = Helpers.color_funktion(e1, e2, original)
                    candidates.append((e1,e2,c))
                    

        #cv2.imshow("contours", img)
        #cv2.waitKey(0)
        return candidates
    
    @staticmethod
    def get_map_groups(e_big, e_small, image):
        if (e_big[1][0] < e_small[1][0]):
            e_small = e_big
        # get inner circle of Ring -5 pixel
        a = e_small[1][0]/2-5
        b = e_small[1][1]/2-5
        
        maxx = max(b,a)
        black_pix = []
        red_pix = []
        
        def is_black(pix, white_rgb):
            # TODO check real white color values in image
            black_rgb = [0,0,0]
            black_rgb[0] = white_rgb[0]-50
            black_rgb[1] = white_rgb[1]-50
            black_rgb[2] = white_rgb[2]-50
            colors = [([150,150,150], "black"), ([220, 216, 221], "white")]
            distances = map(lambda c: (Helpers.distance_euclid(pix, c[0], 2), c[1]), colors)
            return min(distances, key = lambda d: d[0])[1] == "black"
            
        # get white avg color from 4 points
        four_points = [(int(e_big[0][1]+maxx), int(e_big[0][0])), (int(e_big[0][1]-maxx),int(e_big[0][0])),(int(e_big[0][1]), int(e_big[0][0]+maxx)), (int(e_big[0][1]), int(e_big[0][0]-maxx))]
        white_rgb= [0,0,0]
        for p in four_points:
            white_rgb[0] += image[p[0]][p[1]][0]
            white_rgb[1] += image[p[0]][p[1]][1]
            white_rgb[2] += image[p[0]][p[1]][2]
        white_rgb[0] /= 4
        white_rgb[1] /= 4
        white_rgb[2] /= 4
        #print("white: "+ str(white_rgb))
        
        cluster_count = 0
        dic_pix = {}
        dic_group = {}
        
        for x in range(int(e_big[0][0]-maxx),int(e_big[0][0]+maxx)+1):
            for y in range(int(e_big[0][1]-maxx),int(e_big[0][1]+maxx)+1):
                # checks if pixel is in inner cicle 
                if Helpers.ellipse(x, y, e_small[0][0], e_small[0][1], a, b, math.radians(e_small[2])) < 1\
                   and is_black([image[y][x][0],image[y][x][1],image[y][x][2]], white_rgb):
                    # propobly slow -> c++ ?
                     neighbour_group = []
                     if (x-1,y) in dic_pix.keys():
                        neighbour_group.append(dic_pix[(x-1,y)])
                     if (x-1,y-1) in dic_pix.keys():
                        neighbour_group.append(dic_pix[(x-1,y-1)])
                     if (x,y-1) in dic_pix.keys():
                        neighbour_group.append(dic_pix[(x,y-1)])
                     if (x+1,y-1) in dic_pix.keys():
                        neighbour_group.append(dic_pix[(x+1,y-1)])
                     
                     if len(neighbour_group) == 0:
                        dic_pix[(x,y)] = cluster_count
                        dic_group[cluster_count] = [(x,y)]
                        cluster_count += 1
                     else:
                        dic_pix[(x,y)] = neighbour_group[0]
                        dic_group[neighbour_group[0]].append((x,y))
                        if len(neighbour_group) > 1 and len(neighbour_group) < 5:
                            smallest_group = 9999
                            # pick smalest group number
                            for i in neighbour_group:
                                if i < smallest_group: smallest_group = i
                            # save all connected groups/points in dic_group[smallest_group]
                            for i in neighbour_group:
                                if i != smallest_group:
                                    for j in range(len(dic_group[i])):
                                        dic_pix[dic_group[i][j]] = smallest_group
                                        dic_group[smallest_group].append(dic_group[i][j])
                                        
                            # delete groups wich are not needed anymore
                            for i in neighbour_group:
                                if i != smallest_group:
                                    del dic_group[i]
                             
        if len(dic_group) < 3 or len(dic_group) > 5 :
            print("there is no map")
            return None
        else:
            # check for the 3 biggest groups 
            longest= (0,0)
            second_longest = (1,0)
            third_longest = (2,0)
            for i in dic_group.keys():
                length = len(dic_group[i])
                if length > longest[1]:
                    third_longest = second_longest
                    second_longest = longest
                    longest = (i,length)
                elif length > second_longest[1]:
                    third_longest = second_longest
                    second_longest = (i,length)
                elif length > third_longest[1]:
                    third_longest = (i,length)
            # connect longest and secondlongest -> map + box
            for j in range(len(dic_group[second_longest[0]])):
                dic_group[longest[0]].append(dic_group[second_longest[0]][j])
            # third_longest -> cross
            
            return dic_group[longest[0]], dic_group[third_longest[0]]
        
    @staticmethod
    #gets map form image. Red pixels are used to decide if there is a map.
    def get_black_points_from_map(map_image):
        black_pix = []
        red_pix = []
            
        for x in range(len(map_image[0])):
            for y in range(len(map_image)):
                if map_image[y][x] == 0:
                    black_pix.append((x,y))
        return black_pix
        
    @staticmethod
    def get_map_vectors(black_pix, red_pix):
        # check for the moste remote point from the center 
        avg_black = [0,0]
        num_black = len(black_pix)
        for i in range(num_black):
            avg_black[0] += black_pix[i][0]
            avg_black[1] += black_pix[i][1]
        avg_black[0] /= num_black
        avg_black[1] /= num_black
        #print(avg_black)
        max_black_distance = 0
        max_black_distance_point = [0,0]
        for i in range(num_black):
            if Helpers.distance_euclid_2(black_pix[i],avg_black,2) > max_black_distance:
                max_black_distance = Helpers.distance_euclid_2(black_pix[i],avg_black,2)
                max_black_distance_point = black_pix[i]
        
        # check red center 
        avg_red = [0,0]
        num_red = len(red_pix)
        for i in range(num_red):
            avg_red[0] += red_pix[i][0]
            avg_red[1] += red_pix[i][1]
        avg_red[0] /= num_red
        
        
        
        
        avg_red[1] /= num_red
        
        # get normalized vector from avg_black(center) to avg_red and max_black_distance_point
        max_black_norm = Helpers.distance_euclid_2(max_black_distance_point, avg_black, 2)
        max_black_vector = [0,0]
        avg_red_vector = [0,0]
        if max_black_norm != 0:
            # max_black_norm = 1 # for real map
            max_black_vector[0] = (max_black_distance_point[0] - avg_black[0]) / max_black_norm
            max_black_vector[1] = (max_black_distance_point[1] - avg_black[1]) / max_black_norm
        
            avg_red_vector[0] = (avg_red[0] - avg_black[0]) / max_black_norm
            avg_red_vector[1] = (avg_red[1] - avg_black[1]) / max_black_norm

        """
        # plot points
        for i in range(len(black_pix)):
            plt.scatter(black_pix[i][0], black_pix[i][1], c = "black")
        for i in range(len(red_pix)):
            plt.scatter(red_pix[i][0], red_pix[i][1], c ="red")
        plt.scatter(avg_red[0], avg_red[1], c ="yellow")
        plt.scatter(max_black_distance_point[0], max_black_distance_point[1], c ="blue")
        plt.scatter(avg_black[0], avg_black[1], c = "grey")
        plt.show()
        """
        
        return [max_black_vector[0], max_black_vector[1] * -1], [avg_red_vector[0], avg_red_vector[1] * -1] # for real map avg_black
     
    @staticmethod
    def get_map_pose(max_black_vector, avg_red_vector, map_black_vector, map_avg_black_center, ringObject):
        # map_black_vector = [30, -47], map_avg_black_center = [275, 231]
        # get angle between max_black_vector and map_black_vector
        def get_angle(a,b):
            def norm(a):
                return math.sqrt(a[0]**2 + a[1]**2)

            divider = norm(a)*norm(b)
            if divider == 0:
                print("Map warning division by zero", a, b)
                divider = 0.001
            
            def orientation(a,b,c):
                 v_ab1= b[0]-a[0]
                 v_ab2= b[1]-a[1]
                 v_ab3= 0
                 v_ac1= c[0]-a[0]
                 v_ac2= c[1]-a[1]
                 v_ac3= 0
                 
                 cp_3 = v_ab1*v_ac2-v_ab2*v_ac1
                 
                 if cp_3 >= 0:
                     return 1
                 else:
                     return -1
                 
            angle = math.acos((a[0]*b[0] + a[1]*b[1]) / divider) * orientation([0,0], a, b)
            return angle

        
        # rotate avg_red_vector COUNTERCLOCKWISE
        # TODO check signum of angle
        angle = get_angle(max_black_vector, map_black_vector)
        #print(math.degrees(angle))
        normalized_red_vector = [0, 0]
        normalized_red_vector[0] = avg_red_vector[0] * math.cos(angle) - avg_red_vector[1] * math.sin(angle)
        normalized_red_vector[1] = avg_red_vector[0] * math.sin(angle) + avg_red_vector[1] * math.cos(angle)
        
        # get rigth length of red vector. Maybe implement length as constant
        length = Helpers.distance_euclid_2(map_black_vector, [0,0], 2)
        #print length
        normalized_red_vector[0] *= length
        normalized_red_vector[1] *= length

        #print(math.degrees(get_angle(avg_red_vector, normalized_red_vector)))
        
        # tranform treasure location form the map into the goal coordinate frame 
        treaure_map = [map_avg_black_center[0] + normalized_red_vector[0], map_avg_black_center[1] + normalized_red_vector[1]]
        #treaure_map = [map_avg_black_center[0] + map_black_vector[0], map_avg_black_center[1] + map_black_vector[1]]
        # not everything is needed here (direction (0,1) and origin(0,0))
        candidates = np.array([[treaure_map[0], 0, 0], 
                               [treaure_map[1], 1, 0]])
        
        candidates = candidates * ringObject.map_resolution * (-1) # back to meters in picture
        
        candidates = np.concatenate((candidates, np.array([[0,0,0], [1,1,1]])), axis=0)
        
        map_candidates = np.dot(ringObject.transformation_matrix_back, candidates) # back to map space
        treaure = map_candidates[0:3,0]
        treaure_direction = map_candidates[0:3,1]
        
        def getQuaternionRotateFromTo(origin_vector, dest_vector):
            # see MarkerPoint
            v2 = np.array([dest_vector])
            v1 = np.array([origin_vector])
            a = np.cross(v1, v2)
            l1 = np.linalg.norm(v1)
            l2 = np.linalg.norm(v2)
            w = math.sqrt(l1**2 * l2**2) + np.dot(v1, np.transpose(v2))
            return [a[0,0], a[0,1], a[0,2], w[0,0]]
        
        quaternion = getQuaternionRotateFromTo([1, 0, 0], treaure_direction)
        treaure_pose = Pose()
        treaure_pose.position.x = treaure[0]
        treaure_pose.position.y = treaure[1]
        treaure_pose.position.z = 0
        treaure_pose.orientation.x = quaternion[0]
        treaure_pose.orientation.y = quaternion[1]
        treaure_pose.orientation.z = quaternion[2]
        treaure_pose.orientation.w = quaternion[3]
        
        return treaure_pose
        
    @staticmethod
    def color_funktion(e_big, e_small, image):
        if (e_big[1][0] < e_small[1][0]):
            tmp = e_big
            e_big = e_small
            e_small = tmp
        
        a_min = (e_small[1][0]+e_big[1][0])/4-1
        a_max = (e_small[1][0]+e_big[1][0])/4+1
        b_min = (e_small[1][1]+e_big[1][1])/4-1
        b_max = (e_small[1][1]+e_big[1][1])/4+1
        
        maxx = max(b_max,a_max)
        avg_pix = [0,0,0]
        num_pix = 0.0
            
        for x in range(int(e_big[0][0]-maxx),int(e_big[0][0]+maxx)+1):
            for y in range(int(e_big[0][1]-maxx),int(e_big[0][1]+maxx)+1):
                
                if Helpers.ellipse(x, y, e_small[0][0], e_small[0][1], a_min, b_min, math.radians(e_small[2])) > 1 and \
                   Helpers.ellipse(x, y, e_big[0][0], e_big[0][1], a_max, b_max, math.radians(e_big[2])) < 1:
                   
                    num_pix += 1
                    avg_pix[0] += image[y][x][0]
                    avg_pix[1] += image[y][x][1]
                    avg_pix[2] += image[y][x][2]
                    
                    
        avg_pix[0] /= num_pix
        avg_pix[1] /= num_pix
        avg_pix[2] /= num_pix
        
        def color_evaluation(pix):
            colors = [([75,56,160],"red"), ([72,40,26], "blue"), ([80,120,80], "green"), ([0,0,0], "black"), ([60,150,175], "yellow")]
            distances = map(lambda c: (Helpers.distance_euclid(pix, c[0], 2), c[1]), colors)
            return min(distances, key = lambda d: d[0])[1]
        return color_evaluation(avg_pix)
    
    @staticmethod
    def color_funktion_3D(e_big, e_small, image):
        if (e_big[1][0] < e_small[1][0]):
            tmp = e_big
            e_big = e_small
            e_small = tmp
            
        a_min = (e_small[1][0]+e_big[1][0])/4
        a_max = (e_small[1][0]+e_big[1][0])/4+0.5
        b_min = (e_small[1][1]+e_big[1][1])/4
        b_max = (e_small[1][1]+e_big[1][1])/4+0.5
        
        maxx = max(b_max,a_max)
        avg_pix = [0,0,0]
        num_pix = 0.0
        
        def color_evaluation(pix):
            #colors = [([50,35,130],"red"), ([125,50,43], "blue"), ([86,97,58], "green"), ([81,81,81], "black")] #without yellow BGR
            colors = [([57,34,122],"red"), ([120,71,71], "blue"), ([84,94,60], "green"), ([73,64,64], "black")] #without yellow BGR
            distances = map(lambda c: (Helpers.distance_euclid(pix, c[0], 2), c[1]), colors)
            return min(distances, key = lambda d: d[0])[1]
        
        for x in range(int(e_big[0][0]-maxx),int(e_big[0][0]+maxx)+1):
            for y in range(int(e_big[0][1]-maxx),int(e_big[0][1]+maxx)+1):
                
                if Helpers.ellipse(x, y, e_small[0][0], e_small[0][1], a_min, b_min, math.radians(e_small[2])) > 1 and \
                   Helpers.ellipse(x, y, e_big[0][0], e_big[0][1], a_max, b_max, math.radians(e_big[2])) < 1:
                    
                   num_pix += 1
                   avg_pix[0] += image[y][x][0]
                   avg_pix[1] += image[y][x][1]
                   avg_pix[2] += image[y][x][2]
        
        avg_pix[0] /= num_pix
        avg_pix[1] /= num_pix
        avg_pix[2] /= num_pix
        print("bgr: ",avg_pix)       
        return color_evaluation(avg_pix)
        
    @staticmethod
    def color_funktion_cylinder(image):
        dims = image.shape
        num_pix = 0
        avg_pix = [0,0,0]
        for y in range(0, dims[0]):
            for x in range(0, dims[1]):
                num_pix += 1
                avg_pix[0] += image[y][x][0]
                avg_pix[1] += image[y][x][1]
                avg_pix[2] += image[y][x][2]
                
        avg_pix[0] /= num_pix
        avg_pix[1] /= num_pix
        avg_pix[2] /= num_pix
        #print(avg_pix)     
        
        def color_evaluation(pix): 
            colors = [([44,36,97],"red"), ([142,116,107], "blue"), ([80,156,178], "yellow"), ([48,80,60], "green"), ([170,170,170], "wall"), ([200,200,200], "wall") ] #without yellow BGR
            distances = map(lambda c: (Helpers.distance_euclid(pix, c[0], 2), c[1]), colors)
            return min(distances, key = lambda d: d[0])[1]
            
        return color_evaluation(avg_pix)

    @staticmethod
    def extract_distance(e_big,e_small, depth_image):
        if (e_big[1][0] < e_small[1][0]):
            tmp = e_big
            e_big = e_small
            e_small = tmp
        
        a_min = e_small[1][0]/2
        a_max = e_big[1][0]/2
        b_min = e_small[1][1]/2
        b_max = e_big[1][1]/2
        
        maxx = max(b_max,a_max)
        distances = []
        
        for x in range(int(e_big[0][0]-maxx),int(e_big[0][0]+maxx)+1):
            for y in range(int(e_big[0][1]-maxx),int(e_big[0][1]+maxx)+1):
                
                if Helpers.ellipse(x, y, e_small[0][0], e_small[0][1], a_min, b_min, math.radians(e_small[2])) > 1 and \
                   Helpers.ellipse(x, y, e_big[0][0], e_big[0][1], a_max, b_max, math.radians(e_big[2])) < 1:
                    
                    distances.append(depth_image[y][x])
	    
        return np.median(distances)
