#!/usr/bin/env python

import math
from geometry_msgs.msg import Pose

from corner_point import CornerPoint
from task1.msg import ApproachPoint

class CornerHelper:
    
    def __init__(self):
        self.cornerPoints = []
        self.APPROACH_DISTANCE = 0.6 # meters?
        # Add approach points (x,y,quaternion)
        # 4 - box (the one with 4 approach points)
        self.cornerPoints.append(CornerPoint(-1.451,0.7606,[0,0,-0.5933,0.8049]))#N
        self.cornerPoints.append(CornerPoint(-1.7713,-0.6744,[0,0, 0.108,0.994]))#W
        self.cornerPoints.append(CornerPoint(-0.2265,-0.1032,[0,0,0.998,-0.059]))#E
        self.cornerPoints.append(CornerPoint(-0.6920,-1.1337,[0,0,0.8206,0.5713]))#S
        # 3 - box        
        self.cornerPoints.append(CornerPoint(0.3951,-0.1541,[0,0,-0.5344,0.8451]))#N
        self.cornerPoints.append(CornerPoint(-0.1508,-1.3723,[0,0,0.1206,0.9926]))#W
        self.cornerPoints.append(CornerPoint(1.7123,-0.7328,[0,0,0.9755,-0.2199]))#E
        # 1 - box      
        self.cornerPoints.append(CornerPoint(-1.155,-0.9896,[0,0,-0.6049,0.7962]))#N
        
        # Add approach point correspondences
        # 4 - box
        self.cornerPoints[0].addCorrespondingPoint(-0.9662,-0.1376)#N
        self.cornerPoints[1].addCorrespondingPoint(-1.3008,-0.2666)#W
        self.cornerPoints[2].addCorrespondingPoint(-0.7901,-0.4133)#E
        self.cornerPoints[3].addCorrespondingPoint(-1.1273,-0.4962)#S
        # 3 - box
        self.cornerPoints[4].addCorrespondingPoint(0.8853,-0.7789)#N
        self.cornerPoints[5].addCorrespondingPoint(0.5427,-0.9270)#W
        self.cornerPoints[6].addCorrespondingPoint(1.0198,-1.1270)#E
        # 1 - box
        self.cornerPoints[7].addCorrespondingPoint(-0.7458,-1.6996)#N
        
    
    #sprejme x in in y tocke, ki mo jo nasli (3d ring) in vrne najblizjo corner tocko
    def getClosestPoint(self, x, y):
        closestPoint = [100,100]
        selectedCorner = self.cornerPoints[0]
    
        for cornerPoint in self.cornerPoints:
            for correspondingPoint in cornerPoint.correspondingPoints:
                if(self.distance(correspondingPoint, [x,y]) < self.distance(closestPoint, [x,y])):
                    closestPoint = correspondingPoint
                    selectedCorner = cornerPoint
                    
        return self.getPose(selectedCorner)
        
    def distance(self,p1,p2):
        return math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)
    
    def getPose(self, corner):
        pose = Pose()
        pose.position.x = corner.x
        pose.position.y = corner.y
        pose.position.z = 0
        pose.orientation.x = corner.quaternion[0]
        pose.orientation.y = corner.quaternion[1]
        pose.orientation.z = corner.quaternion[2]
        pose.orientation.w = corner.quaternion[3]
        return pose
        
        

