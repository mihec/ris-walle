class CornerPoint:
    
    def __init__(self,x,y,quaternion):
        self.x = x
        self.y = y
        self.quaternion = quaternion
        self.correspondingPoints = []
    
    def addCorrespondingPoint(self,x,y):
        self.correspondingPoints.append([x,y])
