import rospy

class Goal_Manager:

    def __init__(self, tf_buf):
        self.tf_buf = tf_buf
        # Progress flags
        self.haveCylinderColor = False
        self.appproachedCylinder = False
        self.pickedUp3DRing = False
        self.foundFinalLocation =  False
        self.finished = False
        
        self.RING_DISTANCE_DISCOUNT = 3
        self.final_locations = {}
        
        self.cylinder_color = 'undefined'
        self.map_and_3d_color = 'undefined'
        self.final_location = None # this is going to be the final goal we have to send

    def selectNextGoal(self, goals):
        if self.finished:
            rospy.signal_shutdown("Finished ... Wall-e finished his task")
            
        elif self.foundFinalLocation: # when we have the final location in the goal_manager
            self.finished = True
            print("GM: we have already reached the final location")
            return final_location
            
        elif self.pickedUp3DRing: # when we picked up a 3d ring, we look around for the map, we do that automatically, eventually the correct color will be added when we receive it in the callback in move_around from detector_content
            mapPoint = self.getFinalLocation(goals)
            if(mapPoint != None):
                print("GM: final location (mapPoint)")
                return mapPoint
            else:
                return self.selectClosestGoal(goals)
                
        elif self.appproachedCylinder: # when we approached a cylinder
            triD_ring = self.get3DRingPoint(goals)
            if(triD_ring != None):
                print("GM: Returning the 3d ring location!")
                return triD_ring
            else:
                return self.selectClosestGoal(goals)
                
        elif self.haveCylinderColor: # when we classified the cylinder color
            cylinder_point = self.getCylinderPoint(goals)
            if(cylinder_point != None):
                return cylinder_point
            else:
                #print("GM: I don't have the cylinder colour yet, but I am looking for it.")
                return self.selectClosestGoal(goals)
                
        else: #default
            return self.selectClosestGoal(goals)
        
    def getFinalLocation(self, goals):
        non_visited_goals = self.filter_goals(goals)
        
        #check if we already have it in the final locations
        for color, goal in self.final_locations.items():
            if color == self.map_and_3d_color: # we have a final location of the correct color
                return goal
        
        #check if we have any approach points and approach them to see if they have a map
        for id, goal in self.final_locations.items():
            if goal.goal_type == 1 and goal.color == self.map_and_3d_color: #goal is a wall point
                return goal
        
        return None
        
    def get3DRingPoint(self, goals):
        non_visited_goals = self.filter_goals_non_exploration(goals)
        
        for id, goal in non_visited_goals.items():
            if goal.goal_type == 3 and goal.color == self.map_and_3d_color: #goal is a 3d ring
                return goal
        
        return None
        
    def getCylinderPoint(self, goals):
        non_visited_goals = self.filter_goals_non_exploration(goals)
        
        for id, goal in non_visited_goals.items():
            if goal.goal_type == 4 and goal.color == self.cylinder_color: #goal is a cylinder
                return goal
        
        return None
        
    def resetGoal(self, goal_id, goals):
        print("GM: Reseting goal ", goal_id)
        goals[goal_id].visited = False
        
        return goals
        
    def filter_goals_non_exploration(self, goals):
        # we filter out all the exploration goals
        non_visited_goals = {k: v for k, v in goals.iteritems() if (v.visited == False and not v.isExplorationPoint())}
        return non_visited_goals
        
    def filter_goals(self, goals):
        #filter the visited goals, corner and cylinder points
        non_visited_goals = {k: v for k, v in goals.iteritems() if (v.visited == False and not v.isCornerPoint() and not v.isCylinderPoint())}
        
        if self.appproachedCylinder: #if we have the map color then we filter out those 2d rings that don't have the colour we're looking for
            non_visited_goals = {k: v for k, v in goals.iteritems() if (not (v.isApproachPoint == True and not v.color == self.map_and_3d_color))}
        
        return non_visited_goals
        
    def distance(self, roboloc, goal):
        return ((roboloc[0] - goal.position.x)**2 + (roboloc[1] - goal.position.y)**2)**0.5
        
    def selectClosestGoal(self, goals):
        #get the location of the robot in real time
        trans = self.tf_buf.lookup_transform('map', 'base_link', rospy.Time(), rospy.Duration(1.))
        robot_location = [trans.transform.translation.x, trans.transform.translation.y]
        
        non_visited_goals = self.filter_goals(goals)
        
        min_distance = 10000
        next_goal_id = None
        
        for id, goal in non_visited_goals.items():
            new_distance = self.distance(robot_location, goal)
            
            if not goal.isExplorationPoint(): # prioritizing
                new_distance -= self.RING_DISTANCE_DISCOUNT
            
            if new_distance < min_distance:
                min_distance = new_distance
                next_goal_id = id
        
        if next_goal_id == None:
            print("No goal was selected in selectClosestGoal")
            return None
        
        return goals[next_goal_id]
        
    def setCylinderColor(self, c_color):
        print("GM: We have the cylinder color ", c_color)
        self.cylinder_color = c_color
        self.haveCylinderColor = True
        
    def setMapAnd3DColor(self, map_color):
        print("GM: We are now looking for the 3d ring and map with color ", map_color)
        self.map_and_3d_color = map_color
        self.appproachedCylinder = True
        
    def setPicked3dFlag(self):
        print("GM: We have picked up the 3d ring")
        self.pickedUp3DRing = True
        
    def setFinalLocation(self, final_loc):
        print("GM: We have the final location at: ", final_loc)
        self.final_loc = final_loc
        self.foundFinalLocation = True
    
    def endThePain(self):
        print("Goodbye cruel world!")
        self.finished = True
