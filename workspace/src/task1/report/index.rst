.. Robot LOTR documentation master file, created by
   sphinx-quickstart on Tue Jun 18 16:12:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   
.. autoclass:: image_library.Helpers
   :members:

.. autoclass:: marker_point.MarkerPoint
   :members:

.. autoclass:: marker_point_cylinder.MarkerPointCylinder
   :members:

.. autoclass:: detect_rings.The_Ring
   :members:

.. autoclass:: detect_3d_ring.Ring3D
   :members:

.. autoclass:: detect_cylinders.The_Cylinder
   :members:

.. autoclass:: detector_content.The_Rectifier
   :members:

.. autoclass:: goal_manager.Goal_Manager
   :members:

.. autoclass:: move_around.The_Mover
   :members:      
      
.. autoclass:: grasp_ring.Grasper
   :members:
